<?php
/**
 * @file
 * views_filter_object_example.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function views_filter_object_example_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'paragraphs_item|views_filter_object_example|default';
  $ds_fieldsetting->entity_type = 'paragraphs_item';
  $ds_fieldsetting->bundle = 'views_filter_object_example';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'views_filter_object_view' => array(
      'weight' => '3',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:5:{s:13:"optionset_key";s:8:"articles";s:7:"context";s:36:"argument_entity_id:paragraphs_item_1";s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";s:22:"override_title_heading";s:2:"h2";}s:4:"type";s:22:"views_filter_object_ct";s:7:"subtype";s:22:"views_filter_object_ct";}',
        'load_terms' => 1,
      ),
    ),
  );
  $export['paragraphs_item|views_filter_object_example|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function views_filter_object_example_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'views_filter_object_view';
  $ds_field->label = 'Views filter object View';
  $ds_field->field_type = 7;
  $ds_field->entities = array(
    'paragraphs_item' => 'paragraphs_item',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'default' => array(),
    'settings' => array(
      'show_title' => array(
        'type' => 'checkbox',
      ),
      'title_wrapper' => array(
        'type' => 'textfield',
        'description' => 'Eg: h1, h2, p',
      ),
      'ctools' => array(
        'type' => 'ctools',
      ),
    ),
  );
  $export['views_filter_object_view'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function views_filter_object_example_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'paragraphs_item|views_filter_object_example|default';
  $ds_layout->entity_type = 'paragraphs_item';
  $ds_layout->bundle = 'views_filter_object_example';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_vfo_display',
        1 => 'field_vfo_count',
        2 => 'field_vfo_tag',
        3 => 'views_filter_object_view',
      ),
    ),
    'fields' => array(
      'field_vfo_display' => 'ds_content',
      'field_vfo_count' => 'ds_content',
      'field_vfo_tag' => 'ds_content',
      'views_filter_object_view' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['paragraphs_item|views_filter_object_example|default'] = $ds_layout;

  return $export;
}
