<?php
/**
 * @file
 * CTools plugin. Contains the Views Filter Object plugin.
 */

/*
 * Describe Plugin
 */
$plugin = array(
  'title' => t('Views Filter Object'),
  'description' => t('Display a configured view according to field values on an entity.'),
  'single' => TRUE,
  'content_types' => array('views_filter_object_ct'),
  'render callback' => 'views_filter_object_ct_render',
  'required context' => new ctools_context_required(t('Parent entity'), 'entity'),
  'edit form' => 'views_filter_object_ct_edit_form',
  'category' => array(t('Views filter object'), -9),
);

/**
 * Ctools edit form.
 *
 * @param array $form
 *    Form array.
 * @param array $form_state
 *    Form state array.
 *
 * @return array
 *    Form array.
 */
function views_filter_object_ct_edit_form(array $form, array &$form_state) {
  $conf = $form_state['conf'];

  module_load_include('module', 'views_filter_object');

  // Pick option set.
  $form['optionset_key'] = array(
    '#type' => 'select',
    '#title' => t('Option set'),
    '#required' => TRUE,
    '#options' => views_filter_object_get_optionsets_options(),
    '#default_value' => !empty($conf['optionset_key']) ? $conf['optionset_key'] : '',
  );

  return $form;
}

/**
 * Ctools edit form submit handler.
 *
 * @param array $form
 *    Form array.
 * @param array $form_state
 *    Form state array.
 */
function views_filter_object_ct_edit_form_submit(array $form, array &$form_state) {
  foreach (array('optionset_key') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render callback function.
 *
 * @param string $subtype
 *   See ctools content type.
 * @param array $conf
 *   Configuration as done at admin time.
 * @param array $args
 *   See ctools content type.
 * @param ctools_context $context
 *   See ctools content type.
 *
 * @return object
 *    Block object.
 */
function views_filter_object_ct_render($subtype, array $conf, array $args, ctools_context $context) {
  // Check if parent entity is present.
  if (in_array('entity', $context->type) && (!empty($context->data))) {
    $entity = $context->data;
    $block = new stdClass();

    $block->content = theme('views_filter_object_view', array(
        'optionset_key' => $conf['optionset_key'],
        'entity' => $entity,
      )
    );

    return $block;
  }
}
