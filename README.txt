
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Optionsetet Hook
 * Views Filter Object ctools Content Type
 * Design Decisions


INTRODUCTION
------------

Current Maintainers: murrayw <murray@morpht.com>, radimklaska <radim@morpht.com>

Views Filter Object is a module which provides the glue between a View and a
fieldable entity which holds configuration and filtering inforamtion for the
view. This module makes it possible to design user interfaces for content
creators where the content creator can crate an entity, such as a Paragraph
Item, which can configure a View which is then rendered to the page.

This is a simple yet powerful idea. It allows the power and flexibility of
Views to be exposed to users directly. When combined with a module such as
Paragaphs it becomes possible for users to quickly build pages which embed
Views configured by themselves on the node edit page.

To make this more concrete, a Paragraph could be used to configure the
the following aspects of a view:
- the view itself,
- the view display,
- the number of items returned
- a content type filter
- a taxonomy term filter
- a text query.


INSTALLATION
------------

Initial installation is as per normal:

1. Download module to sites/SITENAME/modules/

2. Enable the module.

Once this has been complete there are a few more important steps.

1. Define a view with a display. Add some arguments if desired.

2. Create an entity bundle, such as a Paragraph bundle, which holds fields which
map across to the arguments and properties of the view you just created. See
detailed examples below.

3. Implement a hook_views_filter_object_optionsets() hook which returns an
associative array of one or more "optionsets" which configure the mapping
between view and entity.

4. Configure the display for the entity to use the Views Filter Object ctools
Content Type. This will allow you to select the correct optionset which was
defined in the previous step.

- Go to /admin/structure/ds/fields
- "Add a dynamic field"
- Give it a name and check "Paragraphs item".
- /admin/structure/paragraphs/YOUR-PARA-NAME/display
- Select any Display Suite based layout
- Your new dynamic field should be now available.
- Configure "Format" on new dynamic field
  and "Select content" - Choose "Views filter object".

5. Create an instance of the entity and fill in the fields. View the entity. You
should see a view rendered.

These steps are involved. The aim is to allow site builders to provide a
superior interface to content creators. This requires a little bit or wiring to
achieve.

OPTIONSSET HOOK
---------------


VIEWS FILTER OBJECT CTOOLS CONTENT TYPE
---------------------------------------


DESIGN DECISIONS
----------------
