<?php
/**
 * @file
 * Describe hooks provided by the Views Filter Object module.
 */

/**
 * @defgroup views_filter_object_hooks Views Filter Object Hooks
 * @{
 * hook_views_filter_object_optionsets() is required to get this module
 * working.
 * @}
 */

/*
 * Define available option sets.
 *
 * At least one option set must be defined to get this module working.
 *
 * Option set provides some basic settings and is mapping values in fields to
 * values in views. This way we can allow end users to access very specific
 * parts of views interface in very user friendly way.
 *
 * @See views_filter_object_optionset_get_active_arg_value() to get more info
 * about array structure for fields:
 * @code
 *    'display' => array(
 *      'type' => 'field',
 *      'field' => array('field_vfo_display', 'value', FALSE),
 *    ),
 * @endcode
 *
 * Following example is used in Views Filter Object Example module.
 *
 * @return
 *   An array of option sets See comments in code.
 */
function hook_views_filter_object_optionsets() {
  $optionsets = array();

  // Machine name must be unique.
  $optionsets['articles'] = array(
    // Human readable name.
    'title' => 'Articles by post date',
    // Basic settings.
    'settings' => array(
      // Machine name of a view that will be embedded.
      'view' => 'views_filter_object_example',
      // View display will be set using a field.
      'display' => array(
        'type' => 'field',
        'field' => array('field_vfo_display', 'value', FALSE),
      ),
      'count' => array(
        'type' => 'field',
        'field' => array('field_vfo_count', 'value', FALSE),
      ),
      // Offset will be set in code.
      'offset' => 0,
      // Paging starts with 0, requires pager enabled in Views UI.
      'page' => 0,
    ),
    'args' => array(
      // Pass args from Tag (Entity reference) field to view.
      array(
        'type' => 'field',
        'field' => array('field_vfo_tag', 'target_id', TRUE),
      ),
      // Here are some other possible entries:
      // @See views_filter_object_optionset_get_active_arg_value()
      //
      // Get value from field:
      // array('type' => 'field', 'field' => array('field_name', 'slot', FALSE)),
      //
      // Set single value in code:
      // array('type' => 'value', 'value' => 'single value'),
      //
      // Set multi value in code:
      // array('type' => 'value', 'value' => array('my value1', 'my value2', 'my value3')),
    ),
  );

  return $optionsets;
}